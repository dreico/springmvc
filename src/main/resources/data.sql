-- INSERT INTO category (id, code, name, CREATED_ON, VERSION ) VALUES ('10', 'ND','ND_NAME','2018-04-10 14:46:19.282', 0);


insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (21, '2018-08-21 17:16:27', '2018-04-16 10:20:10', 1, 'EUR', 'Garlic - Primerba, Paste');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (22, '2018-08-14 19:44:53', '2018-07-22 06:49:39', 1, 'MXN', 'Blouse / Shirt / Sweater');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (23, '2018-07-28 17:22:51', '2018-08-09 00:46:53', 1, 'EUR', 'Beer - Steamwhistle');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (24, '2017-09-18 12:48:14', '2017-12-12 03:48:20', 1, 'CNY', 'Squash - Sunburst');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (25, '2018-01-07 06:27:39', '2018-03-31 05:45:25', 1, 'USD', 'Tomatoes - Cherry, Yellow');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (26, '2018-08-11 20:38:53', '2018-05-11 23:40:30', 1, 'RUB', 'Coke - Classic, 355 Ml');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (27, '2018-02-28 07:01:02', '2017-11-05 18:26:41', 1, 'BRL', 'Sausage - Liver');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (28, '2018-05-08 21:36:13', '2017-09-16 11:53:59', 1, 'USD', 'Scallops - 20/30');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (29, '2018-07-14 06:33:14', '2018-02-24 22:59:36', 1, 'IDR', 'Bread - Bagels, Plain');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (30, '2017-10-07 10:48:45', '2018-05-05 15:55:27', 1, 'PEN', 'Icecream - Dstk Cml And Fdg');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (31, '2017-12-29 11:37:32', '2018-04-26 12:34:31', 1, 'CZK', 'Ranchero - Primerba, Paste');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (32, '2018-09-06 14:57:09', '2018-06-26 04:42:07', 1, 'CAD', 'Dc Hikiage Hira Huba');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (33, '2018-03-04 02:37:07', '2017-11-12 23:00:17', 1, 'RUB', 'Beer - Camerons Auburn');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (34, '2018-06-10 16:22:52', '2017-09-16 07:55:37', 1, 'VND', 'Aspic - Clear');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (35, '2018-08-22 06:38:21', '2018-01-07 20:39:31', 1, 'EUR', 'Oil - Margarine');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (36, '2017-11-19 20:41:21', '2018-05-05 01:14:28', 1, 'CLP', 'Juice - Ocean Spray Kiwi');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (37, '2018-01-22 21:53:23', '2018-05-22 19:29:01', 1, 'RUB', 'Chutney Sauce');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (38, '2018-07-09 00:09:40', '2017-11-15 19:03:40', 1, 'EUR', 'Tart Shells - Savory, 2');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (39, '2018-02-09 19:13:32', '2017-11-23 15:41:06', 1, 'IDR', 'Juice - Apple, 1.36l');
insert into MOCK_DATA (id, created_on, update_on, version, code, name) values (40, '2018-06-01 13:01:36', '2017-12-02 06:13:10', 1, 'AMD', 'Grapefruit - Pink');