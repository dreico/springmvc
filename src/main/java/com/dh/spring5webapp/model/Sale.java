package com.dh.spring5webapp.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;


@Entity
public class Sale extends ModelBase {

    private int cantidad;

    //@OneToOne(optional = false)
    @OneToOne(targetEntity = Item.class)
    private Item item;

    //@OneToOne(optional=false)
    @OneToOne(targetEntity = Tax.class)
    private Tax tax;

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }
}
