package com.dh.spring5webapp.Bootstrap;

import com.dh.spring5webapp.model.*;
import com.dh.spring5webapp.repositories.*;
import com.dh.spring5webapp.services.EmployeeService;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Locale;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    //private CategoryRepository
    private CategoryRepository categoryRepository;
    private SubCategoryRepository subCategoryRepository;
    private ItemRepository itemRepository;
    private EmployeeRepository employeeRepository;
    private PositionRepository positionRepository;
    private ContractRepository contractRepository;
    private TaxRepository taxRepository;
    private SaleRepository saleRepository;

    public DevBootstrap(CategoryRepository categoryRepository,
                        SubCategoryRepository subCategoryRepository,
                        ItemRepository itemRepository,
                        EmployeeRepository employeeRepository,
                        PositionRepository positionRepository,
                        ContractRepository contractRepository,
                        TaxRepository taxRepository,
                        SaleRepository saleRepository) {
        this.categoryRepository = categoryRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.itemRepository = itemRepository;
        this.employeeRepository = employeeRepository;
        this.positionRepository = positionRepository;
        this.contractRepository = contractRepository;
        this.taxRepository = taxRepository;
        this.saleRepository = saleRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent applicationEvent) {
        initData();
    }

    private void initData() {
        //EPP(equipo de proteccion personal) category
        Category tvCategory = new Category();
        tvCategory.setCode("TV");
        tvCategory.setName("Televisor");

        Category radioCategory = new Category();
        radioCategory.setCode("RADIO");
        radioCategory.setName("Radio");

        categoryRepository.save(tvCategory);
        categoryRepository.save(radioCategory);

        //=========== SUBCATEGORY ======================

        // safety subcategory
        SubCategory samsungSubCategory = new SubCategory();
        samsungSubCategory.setCategory(tvCategory);
        samsungSubCategory.setCode("SAMSUNG");
        samsungSubCategory.setName("Samsung");
        subCategoryRepository.save(samsungSubCategory);


        SubCategory sonySubCategory = new SubCategory();
        sonySubCategory.setCategory(tvCategory);
        sonySubCategory.setCode("SONY");
        sonySubCategory.setName("Sony");
        subCategoryRepository.save(sonySubCategory);

        SubCategory aiwaSubCategory = new SubCategory();
        aiwaSubCategory.setCategory(radioCategory);
        aiwaSubCategory.setCode("AIWA");
        aiwaSubCategory.setName("Aiwa");
        subCategoryRepository.save(aiwaSubCategory);

        //=================== ITEMS ======================
        // televisor sony
        Item tv01 = new Item();
        tv01.setCode("KDL-48W655D");
        tv01.setName("Tv Sony de 48 pulgadas - modelo KDL-48W655D - LED FULL HD Smart Tv,Plano");
        tv01.setDescription("Una imagen mas refinada y limpia");
        tv01.setSubCategory(sonySubCategory);
        tv01.setPrice(800.0f);
        tv01.setDiscount(5f);
        itemRepository.save(tv01);

        // televisor samsung
        Item tv02 = new Item();
        tv02.setCode("UN49K6500");
        tv02.setName("Tv Samnsung de 49 pulgadas - modelo UN49K6500 - LED FULL HD Smart Tv,Curvo");
        tv02.setDescription("Una imagen mas refinada y limpia full hd samsung");
        tv02.setSubCategory(samsungSubCategory);
        tv02.setPrice(400.0f);
        tv02.setDiscount(10f);
        itemRepository.save(tv02);

        // radio Item
        Item radio01 = new Item();
        radio01.setCode("Pro CM9950");
        radio01.setName("LG X-Boom Pro CM9950 Minicomponente Salidas P.M.P.O 50.000W y R.M.S 4.400W");
        radio01.setDescription("Nuevo LG X-Boom Pro CM9950 El Minicomponente Más Potente Del Mundo con 50000 W Salida en (PMPO); en RMS 4400 W Salida.\n" +
                "Cuenta con Doble Puerto USB, tecnologia Bluetooth + NFC para enviar música desde tu Portátil, Tablet o Celular");
        radio01.setSubCategory(aiwaSubCategory);
        radio01.setPrice(500f);
        radio01.setDiscount(15f);
        itemRepository.save(radio01);

        //============EMPLOYEES=================

        // John Employee
        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Doe");

        // Jose Employee
        Employee jose = new Employee();
        jose.setFirstName("jose");
        jose.setLastName("rios");
        employeeRepository.save(jose);


        // Position
        Position position = new Position();
        position.setName("OPERATIVE");
        positionRepository.save(position);

        // contract
        Contract contract = new Contract();
        contract.setEmployee(john);
        contract.setInitDate(new Date(2010, 1, 1));
        contract.setPosition(position);

        john.getContracts().add(contract);
        employeeRepository.save(john);
        contractRepository.save(contract);

        //================== TAX ==================

        Tax tax01 = new Tax();
        tax01.setDate(new Date(2010,1,1));
        tax01.setDetail("Detail about tax 01");
        taxRepository.save(tax01);

        Tax tax02 = new Tax();
        tax02.setDate(new Date(2010,2,2));
        tax02.setDetail("Detail about tax 02");
        taxRepository.save(tax02);

        //============= SALES =====================

        Sale sale01 = new Sale();
        sale01.setCantidad(10);
        sale01.setItem(radio01);
        sale01.setTax(tax01);
        saleRepository.save(sale01);

        Sale sale02 = new Sale();
        sale02.setCantidad(20);
        sale02.setItem(tv01);
        sale02.setTax(tax01);
        saleRepository.save(sale02);

        Sale sale03 = new Sale();
        sale03.setCantidad(20);
        sale03.setItem(tv02);
        sale03.setTax(tax02);
        saleRepository.save(sale03);

        Sale sale04 = new Sale();
        sale04.setCantidad(15);
        sale04.setItem(tv01);
        sale04.setTax(tax02);
        saleRepository.save(sale04);
    }
}
