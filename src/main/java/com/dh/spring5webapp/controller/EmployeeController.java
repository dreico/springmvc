package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.command.LoginCommand;
import com.dh.spring5webapp.model.Employee;


import com.dh.spring5webapp.services.EmployeeService;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class EmployeeController {
    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping("employees")
    public String getEmployees(Model model,HttpSession session) {

        if(session.getAttribute("loggedEmployee") != null) {

            model.addAttribute("employees", employeeService.findAll());
            model.addAttribute("adminMode", session.getAttribute("loggedEmployee"));
            return "admin/users";

        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "index";
        }
    }

    @RequestMapping("employee")
    public String newEmployee(Model model, Employee emp, HttpSession session) {
        if(session.getAttribute("loggedEmployee") != null)
            return "admin/user";
        else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "index";

        }
    }

    @PostMapping("employee")
    public String saveEmployee(Model model, HttpServletRequest request, HttpSession session){

        if(session.getAttribute("loggedEmployee") != null){

            Employee emp;
            String idParam = request.getParameter("id");
            if (idParam != null && idParam != "") {
                Long id = Long.valueOf(idParam);
                emp = employeeService.findById(id);
            } else {
                emp = new Employee();
            }
            emp.setFirstName(request.getParameter("firstName"));
            emp.setLastName(request.getParameter("lastName"));
            employeeService.save(emp);

            return "redirect:/employees";

        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "redirect:/index";
        }
    }

    @RequestMapping("employee/edit/{id}")
    public String getEmployeeById(@PathVariable("id") @NotNull Long id, Model model, HttpSession session) {

        if(session.getAttribute("loggedEmployee") != null) {

            Employee emp = employeeService.findById(id);
            model.addAttribute("employee", emp);
            model.addAttribute("formImage", emp.getImage());
            return "admin/user";

        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "redirect:/index";
        }
    }

    @RequestMapping("employee/delete/{id}")
    public String deleteEmployeeById(@PathVariable("id") @NotNull Long id, Model model, HttpSession session) {

        if(session.getAttribute("loggedEmployee") != null) {

            employeeService.deleteById(id);
            return "redirect:/employees";

        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "redirect:/index";
        }
    }

    @RequestMapping(value={"login","admin"}, method = RequestMethod.GET)
    public String accessAdmin(Model m){
        m.addAttribute("command", new LoginCommand());
        return "login";
    }

    @RequestMapping(value="login", method = RequestMethod.POST)
    public String verifyLogin(@ModelAttribute("loggedEmployee")LoginCommand cmd, Model model,HttpSession session){
//        System.out.println(cmd.getLoginName());
//        System.out.println(cmd.getPassword());
        if(cmd.getLoginName() == null || cmd.getLoginName() == "" || cmd.getPassword() == null || cmd.getPassword() == ""){
            model.addAttribute("command", new LoginCommand());
            return "login";
        }


        Employee loggedUser = employeeService.login(cmd.getLoginName(),cmd.getPassword());

        System.out.println("user"+loggedUser);
        if(loggedUser == null){
            model.addAttribute("command", new LoginCommand());
            model.addAttribute("err", "Login Failled!! Enter a valid credentials");
            return "login";
        }else{
            System.out.println("we are in !!");
            session.setAttribute("loggedEmployee",loggedUser);
            model.addAttribute("logged",loggedUser);
            return "redirect:/employees";
        }
    }



    @RequestMapping(value="logout")
    public String logout(HttpSession session, Model model){
        if(session.getAttribute("loggedEmployee") != null) {
            session.invalidate();
            return "redirect:/index?act=lo";
        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "redirect:/index";
        }
    }

    /**
     * UPLOADING IMAGE
     */

    @RequestMapping(value = "employees/{id}/image")
    public String showUploadItemImageForm(Model model,@PathVariable String id){
        Employee itemPersisted = employeeService.findById(Long.valueOf(id));
        model.addAttribute("employee", itemPersisted);
        return "admin/uploadImage";
    }

    @PostMapping("employees/{id}/image")
    public String potImage(Model model, @PathVariable String id, @RequestParam("imagefile") MultipartFile file){
        long idNumber = Long.valueOf(id);
        employeeService.saveImage( idNumber,file);

//        model.addAttribute("employee", employeeService.findById(idNumber));
//        return "redirect:/employee/edit/"+idNumber;
        return "redirect:/employees";
    }



    @GetMapping("employee/{id}/readimage")
    public void renderImageFromDB(@PathVariable String id, HttpServletResponse response)throws IOException {
        Employee employeePersisted = employeeService.findById(Long.valueOf(id));

        if(employeePersisted.getImage() != null){
            byte[] byteArray = new byte[employeePersisted.getImage().length];
            int i = 0;

            for(Byte wrappedByte : employeePersisted.getImage()){
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(byteArray);
            IOUtils.copy(is, response.getOutputStream());
        }
    }
}