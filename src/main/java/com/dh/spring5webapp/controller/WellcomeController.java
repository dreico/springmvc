package com.dh.spring5webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WellcomeController {

    @RequestMapping("/welcome")
    public String loginMessage(){
        return "welcome";
    }
}
