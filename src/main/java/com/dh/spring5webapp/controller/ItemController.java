/**
 * @author: Edson A. Terceros T.
 */

package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.model.Employee;
import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.model.SubCategory;
import com.dh.spring5webapp.services.ItemService;
import com.dh.spring5webapp.services.SubCategoryService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping("/items")
public class ItemController {

    private ItemService itemService;
    private SubCategoryService subCategoryService;

    public ItemController(ItemService service, SubCategoryService subCategoryService) {
        this.itemService = service;
        this.subCategoryService = subCategoryService;
    }

    @RequestMapping
    public String getItems(Model model, HttpSession session) {

        if(session.getAttribute("loggedEmployee") != null) {

            model.addAttribute("items", itemService.findAll());
            return "admin/items";

        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "index";
        }
    }

//
//    @RequestMapping("/{id}")
//    public String getItemsById(@PathVariable("id") @NotNull Long id, Model model){
//        Item item = itemService.findById(id);
//        model.addAttribute("item",item);
//
//        return "admin/item";
//    }

    @RequestMapping("/new")
    public String newItem(Model model, HttpSession session){

        if(session.getAttribute("loggedEmployee") != null) {

        model.addAttribute("newItem",true);
        model.addAttribute("subcategories",subCategoryService.findAll());
        return "admin/item";

        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "index";
        }
    }

    @PostMapping
    public String saveItem(Model model,  HttpServletRequest request, HttpSession session){

        if(session.getAttribute("loggedEmployee") != null) {

        String idSubCategory = request.getParameter("subcategory");
        //System.out.println("*****************>>>>>>>>>"+idSubCategory);
        SubCategory subCategory = subCategoryService.findById(Long.valueOf(idSubCategory));
        Item item;
        String idParam = request.getParameter("id");
        if (idParam != null && idParam != "") {
            Long id = Long.valueOf(idParam);
            item = itemService.findById(id);
        } else {
            item = new Item();
        }
        item.setName(request.getParameter("name"));
        item.setCode(request.getParameter("code"));
        item.setSubCategory(subCategory);
        itemService.save(item);

        return "redirect:/items";

        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "index";
        }
    }

    @RequestMapping("/update/{id}")
    public String updateItem(Model model, @PathVariable String id, HttpSession session){

        if(session.getAttribute("loggedEmployee") != null) {

        Item item = itemService.findById(Long.valueOf(id));
        model.addAttribute("item", item);
        model.addAttribute("subcategories", subCategoryService.findAll());
        model.addAttribute("subcategoryId",item.getSubCategory().getId());
        return "admin/item";

        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "index";
        }
    }

    @RequestMapping("/delete/{id}")
    public String deleteItem(Model model, @PathVariable String id, HttpSession session){

        if(session.getAttribute("loggedEmployee") != null) {

            itemService.deleteById(Long.valueOf(id));
            return "redirect:/items/";
        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "index";
        }
    }

    @RequestMapping(value = "/{id}/image")
    public String showUploadItemImageForm(Model model,@PathVariable String id, HttpSession session){

        if(session.getAttribute("loggedEmployee") != null) {

            Item itemPersisted = itemService.findById(Long.valueOf(id));
            model.addAttribute("item", itemPersisted);
            return "admin/uploadImageItem";

        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "index";
        }
    }

    @PostMapping("/{id}/image")
    public String postImage(Model model, @PathVariable String id, @RequestParam("imagefile") MultipartFile file, HttpSession session){

        if(session.getAttribute("loggedEmployee") != null) {

            itemService.saveImage(Long.valueOf(id),file);
            itemService.saveImage(Long.valueOf(id),file);

            model.addAttribute("item", itemService.findById(Long.valueOf(id)));
            model.addAttribute("subCategories", subCategoryService.findAll());
            return "redirect:/items/update/{id}";
        }else{
            model.addAttribute("errLogIsNeeded","You need to log in for access at this page");
            return "index";
        }

    }




    @GetMapping("/{id}/readimage")
    public void renderImageFromDB(@PathVariable String id, HttpServletResponse response)throws IOException{
        Item itemPersisted = itemService.findById(Long.valueOf(id));

        if(itemPersisted.getImage() != null){
            byte[] byteArray = new byte[itemPersisted.getImage().length];
            int i = 0;

            for(Byte wrappedByte : itemPersisted.getImage()){
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(byteArray);
            IOUtils.copy(is, response.getOutputStream());
        }
    }
}