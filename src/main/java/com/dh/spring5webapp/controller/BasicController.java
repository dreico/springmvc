package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.command.ResourceNotFoundException;
import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.model.SubCategory;
import com.dh.spring5webapp.repositories.CategoryRepository;
import com.dh.spring5webapp.repositories.ItemRepository;
import com.dh.spring5webapp.repositories.SaleRepository;
import com.dh.spring5webapp.repositories.SubCategoryRepository;
import com.dh.spring5webapp.services.CategoryService;
import com.dh.spring5webapp.services.ItemService;

import com.dh.spring5webapp.services.SaleService;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.PageRequest;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;


@Controller
public class BasicController {



    private ItemService itemService;
    private CategoryService categoryService;
    private SaleService saleService;

    private ItemRepository itemRepository;
    private SubCategoryRepository subCategoryRepository;


    public BasicController(ItemService itemService,
                           ItemRepository itemRepository,
                           CategoryService categoryService,
                           SubCategoryRepository subCategoryRepository,
                           SaleService saleService){
        this.itemService = itemService;
        this.itemRepository = itemRepository;
        this.categoryService = categoryService;
        this.subCategoryRepository = subCategoryRepository;
        this.saleService = saleService;
    }

    @RequestMapping(value={"/","index"})
    public String getIndex(Model model){
        model.addAttribute("items", itemService.findAll());
        model.addAttribute("itemsDiscount", itemService.getTopDiscount(3));
        model.addAttribute("itemsNew", itemService.getNewItems(4));
        model.addAttribute("itemsRandom", itemService.getRandomItems(10));

        return "index";
    }

    @RequestMapping("catalog")
    public String getCatalog(@RequestParam Optional<Integer> page,Model model){
        Page<Object[]> itemPaging = itemRepository.findItemsRandom(new PageRequest(page.orElse(0), 9));

        //model.addAttribute("itemPagingBySub", itemPagingBySub);
        model.addAttribute("itemPaging", itemPaging);
        model.addAttribute("itemsRandomPlus", itemService.getRandomItems(3));
        model.addAttribute("itemsRandom", itemService.getRandomItems(10));
        model.addAttribute("categories", categoryService.findCategoriesAndSize());
        model.addAttribute("itemsBestSeller", saleService.findBestSeller(4) );
        return "catalog";
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public String getItemsBySearch(@RequestParam Optional<Integer> page,Model model,@RequestParam(value="sub",required = false) Integer subCatId){

        Page<Object[]> itemPaging = itemRepository.findItemsBySub(new PageRequest(page.orElse(0), 9),Long.valueOf(subCatId));

        //model.addAttribute("itemPagingBySub", itemPagingBySub);
        model.addAttribute("itemPaging", itemPaging);
        model.addAttribute("idSubCategory", subCatId);
        model.addAttribute("itemsRandomPlus", itemService.getRandomItems(3));
        model.addAttribute("itemsRandom", itemService.getRandomItems(10));
        model.addAttribute("categories", categoryService.findCategoriesAndSize());
        model.addAttribute("itemsBestSeller", saleService.findBestSeller(4) );
        return "search";
    }

    @RequestMapping("details")
    public String getProductDetails(@RequestParam(value = "id", required = false) Integer id,Model model){
        if(id == null){
            return "redirect:/index";
        }

        model.addAttribute("itemsRandomPlus", itemService.getRandomItems(3));
        model.addAttribute("itemsRandom", itemService.getRandomItems(10));
        model.addAttribute("item", itemService.getItemById(id));
        return "product-detail";
    }

    @RequestMapping("contact")
    public String getContact(Model model){
        model.addAttribute("itemsRandom", itemService.getRandomItems(10));
        return "contact";
    }



/*    @RequestMapping(value = "/ajaxtest", method = RequestMethod.GET)
    public @ResponseBody List<Item> getTime(@RequestParam("id") Integer id) {

//        Random rand = new Random();
//        float r = rand.nextFloat() * 100;
//        String result = "<br>Next Random # is <b>" + r + "</b>. Generated on <b>" + new Date().toString() + "</b>";
//        System.out.println("Debug Message from CrunchifySpringAjaxJQuery Controller.." + new Date().toString());
        //return result;
        return itemService.getRandomItems(id);
    }*/

    @RequestMapping(value = "getSubcategories", method = RequestMethod.GET)
    public @ResponseBody List<Object[]> ajaxGetSubcategories(@RequestParam("id") Integer id) {

        return subCategoryRepository.findSubcategoriesAndSize(Long.valueOf(id));
    }

    @RequestMapping(value = "about-us", method = RequestMethod.GET)
    public String getAboutUs(Model model){
        model.addAttribute("itemsRandom", itemService.getRandomItems(10));
        return "about-us";
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleResourceNotFoundException() {
        return "notFound";
    }
}
