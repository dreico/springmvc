package com.dh.spring5webapp.command;


import com.dh.spring5webapp.model.Employee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeMapper implements RowMapper<Employee> {

    public Employee mapRow(ResultSet rs, int arg1) throws SQLException {

        Employee emp = new Employee();

        emp.setFirstName(rs.getString("first_name"));

        emp.setLastName(rs.getString("last_name"));

        return emp;

    }

}