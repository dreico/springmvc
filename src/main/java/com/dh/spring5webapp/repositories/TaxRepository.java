package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Tax;

import org.springframework.data.repository.CrudRepository;

public interface TaxRepository extends CrudRepository<Tax, Long> {
}
