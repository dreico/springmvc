package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.model.Employee;
import com.dh.spring5webapp.model.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    Optional<List<Category>> findByCode(String code);

    @Query( value="SELECT category.id as id, category.name as name, (SELECT count(*) from sub_category WHERE sub_category.category_id = category.id) as subcategories_size from category", nativeQuery = true)
    List<Object[]> findCategoriesAndSize();


}
