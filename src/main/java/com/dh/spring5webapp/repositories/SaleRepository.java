package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Sale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SaleRepository extends CrudRepository<Sale,Long> {

    @Query( value="SELECT item.id, item.name,item.detail,item.price,item.image, (SELECT sum(sale.cantidad) from sale WHERE sale.item_id = item.id) as sales_count from item ORDER BY sales_count desc limit ? ", nativeQuery = true)
    List<Object[]> findBestSeller(Long id);
}
