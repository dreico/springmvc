package com.dh.spring5webapp.repositories;


import com.dh.spring5webapp.model.Item;
import org.springframework.data.domain.Page;


import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

//@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
//
//    @Query("SELECT * FROM item ORDER BY item.discount DESC LIMIT :limit;");
//    public List<Item> findByEmailReturnStream(@Param("limit") int limit);

//    @Query("SELECT i FROM Item i ORDER BY i.name")
//    Page<Item> findByPtm(Pageable pageable);

    @Query(value="SELECT i.* FROM item i, sub_category WHERE i.sub_category_id=sub_category.id and sub_category.id =? ", nativeQuery = true)
    Page<Object[]> findItemsBySub(Pageable pageable, Long sub);

    @Query(value="SELECT * FROM item ORDER BY RAND() ", nativeQuery = true)
    Page<Object[]> findItemsRandom(Pageable pageable);

    Item getItemById(Long id);

    @Query(value = "SELECT * FROM item ORDER BY RAND() LIMIT ?;",nativeQuery = true)
    List<Item> getRandomItems(Long id);
}
