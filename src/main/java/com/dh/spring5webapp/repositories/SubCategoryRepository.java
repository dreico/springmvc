package com.dh.spring5webapp.repositories;


import com.dh.spring5webapp.model.SubCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {

    @Query( value="SELECT sub_category.id, sub_category.name, (SELECT count(*) from item WHERE sub_category.id = item.sub_category_id) as items_size from sub_category where sub_category.category_id=? ", nativeQuery = true)
    List<Object[]> findSubcategoriesAndSize(Long id);
}
