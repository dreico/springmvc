/**
 * @author: Edson A. Terceros T.
 */

package com.dh.spring5webapp.services;



import com.dh.spring5webapp.model.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface ItemService extends GenericService<Item> {
    void saveImage(Long id, MultipartFile file);

    //List<Item> getTop3Discount();

//    @Query(value = "SELECT * FROM todos t WHERE " +
//            "LOWER(t.title) LIKE LOWER(CONCAT('%',:searchTerm, '%')) OR " +
//            "LOWER(t.description) LIKE LOWER(CONCAT('%',:searchTerm, '%'))",
//            nativeQuery = true
//    )
//    @Query(value="SELECT * FROM item ORDER BY item.discount DESC LIMIT (CONCAT('%',:limit, '%')) ",nativeQuery = true)
//    List<Item> findBySearchTermNative(@Param("limit") int limit);

    public List<Item> getTopDiscount(int top);
    public List<Item> getNewItems(int top);
    public List<Item> getRandomItems(int top);
    Page<Item> getRandomItemsPageable(Pageable pageable);

    Item getItemById(int id);


}