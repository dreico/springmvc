package com.dh.spring5webapp.services;





import com.dh.spring5webapp.model.Tax;

import com.dh.spring5webapp.repositories.TaxRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;




@Service
public class TaxServiceImpl extends GenericServiceImpl<Tax> implements TaxService {
    private static Logger logger = LoggerFactory.getLogger(TaxServiceImpl.class);

    private TaxRepository repository;

    public TaxServiceImpl(TaxRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Tax, Long> getRepository() {
        return repository;
    }


}