package com.dh.spring5webapp.services;

import com.dh.spring5webapp.command.EmployeeMapper;
import com.dh.spring5webapp.model.Employee;
import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.repositories.EmployeeRepository;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl extends GenericServiceImpl<Employee> implements EmployeeService{

    private static Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    JdbcTemplate jdbcTemplate;
    private EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    protected CrudRepository<Employee, Long> getRepository() {
        return employeeRepository;
    }

    @Override
    public List<Employee> getEmployees() {
        List<Employee> employees = new ArrayList<>();
        employeeRepository.findAll().forEach(employees::add);

        return employees;
    }

    @Override
    public Employee login(String username, String password)  {
        String sql = "SELECT * FROM EMPLOYEE WHERE first_name='" + username + "' and last_name = '" + password + "';";

        List<Employee> users = jdbcTemplate.query(sql, new EmployeeMapper());

        return users.size() > 0 ? users.get(0) : null;
    }

    @Override
    public void saveImage(Long id, MultipartFile file) {
        Employee employeePersisted = findById(id);
        try {
            //Byte[] bytes = ImageUtils.inputStreamToByteArray(file);
            Byte[] bytes = new Byte[file.getBytes().length];
            int i = 0;
            for (Byte aByte : file.getBytes()){
                bytes[i++] = aByte;
            }
            employeePersisted.setImage(bytes);
            getRepository().save(employeePersisted);
        } catch (IOException e) {
            logger.error("Error reading file", e);
            e.printStackTrace();
        }
    }
}


