package com.dh.spring5webapp.services;


import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.repositories.CategoryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class CategoryServiceImpl extends GenericServiceImpl<Category> implements CategoryService{

    private CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    //@Override
    public List<Category> getCategories(){
        List<Category> categories = new ArrayList<>();
        categoryRepository.findAll().forEach(categories::add);

        return categories;
    }

/*    @Override
    public Category findById(Long id){
        return categoryRepository.findById(id).get();
    }*/

    @Override
    protected CrudRepository<Category, Long> getRepository() {
        return categoryRepository;
    }

    @Override
    public List<Category> find(String code){
        return (org.springframework.util.StringUtils.isEmpty(code)) ? getCategories() : categoryRepository.findByCode(code).get();
    }


    public List<Object[]> findCategoriesAndSize(){
        return categoryRepository.findCategoriesAndSize();
    }
}
