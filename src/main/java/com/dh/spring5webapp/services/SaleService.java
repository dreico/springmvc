package com.dh.spring5webapp.services;


import com.dh.spring5webapp.model.Sale;

import java.util.List;

public interface SaleService extends GenericService<Sale> {

    List<Object[]> findBestSeller(int id);
}