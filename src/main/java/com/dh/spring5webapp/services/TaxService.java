package com.dh.spring5webapp.services;


import com.dh.spring5webapp.model.Tax;


public interface TaxService extends GenericService<Tax> {

}