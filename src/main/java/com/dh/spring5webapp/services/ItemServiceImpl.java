/**
 * @author: Edson A. Terceros T.
 */

package com.dh.spring5webapp.services;



import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.repositories.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ItemServiceImpl extends GenericServiceImpl<Item> implements ItemService {
    private static Logger logger = LoggerFactory.getLogger(ItemServiceImpl.class);

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    JdbcTemplate jdbcTemplate;

    private ItemRepository repository;

    public ItemServiceImpl(ItemRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Item, Long> getRepository() {
        return repository;
    }

    @Override
    public void saveImage(Long id, MultipartFile file) {
        Item itemPersisted = findById(id);
        try {
            //Byte[] bytes = ImageUtils.inputStreamToByteArray(file);
            Byte[] bytes = new Byte[file.getBytes().length];
            int i = 0;
            for (Byte aByte : file.getBytes()){
                bytes[i++] = aByte;
            }
            itemPersisted.setImage(bytes);
            getRepository().save(itemPersisted);
        } catch (IOException e) {
            logger.error("Error reading file", e);
            e.printStackTrace();
        }
    }

    @Override
    public List<Item> getTopDiscount(int top){
        List<Item> items = new ArrayList<>();

        String sql = "SELECT * FROM item ORDER BY item.discount DESC LIMIT ?";

        Query query = entityManager.createNativeQuery(sql, Item.class);
        query.setParameter(1, top);

        return query.getResultList();
    }

    @Override
    public List<Item> getNewItems(int top){
        List<Item> items = new ArrayList<>();

        String sql = "SELECT * FROM item ORDER BY item.created_on DESC LIMIT ?;";

        Query query = entityManager.createNativeQuery(sql, Item.class);
        query.setParameter(1, top);



        return query.getResultList();
    }

//    public List<Item> getRandomItems(int top){
//        List<Item> items = new ArrayList<>();
//        repository.findAllRandom(top).forEach(items::add);
//
//        return items;
//    }
    @Override
    public List<Item> getRandomItems(int top){
//        List<Item> items = new ArrayList<>();
//
//        String sql = "SELECT * FROM item ORDER BY RAND() LIMIT ?;";
//
//        Query query = entityManager.createNativeQuery(sql, Item.class);
//        query.setParameter(1, top);
//
//
//
//        return query.getResultList();
        return repository.getRandomItems(Long.valueOf(top));
    }



    @Override
    public Page<Item> getRandomItemsPageable(Pageable pageable){


        String sql = "SELECT * FROM item ORDER BY RAND();";

        Query query = entityManager.createNativeQuery(sql, Item.class);

        List<Item> itemList = new ArrayList<>();
        itemList = query.getResultList();
        //Page<Item> page = new PageImpl<>(query.getResultList());

        Page<Item> items = new PageImpl<>(query.getResultList(), pageable, query.getResultList().size());

        return items;
    }

    public Item getItemById(int id){
        return repository.getItemById(Long.valueOf(id));
    }


}