package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Employee;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface EmployeeService extends GenericService<Employee>{
    List<Employee> getEmployees();
    Employee login(String userLogin, String password);
    void saveImage(Long id, MultipartFile file);


}
