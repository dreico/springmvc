package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Category;

import java.util.List;

public interface CategoryService extends GenericService<Category> {
  //  List<Category> findByCode(String code);
  //List<Category> getCategories();
  List<Category> find(String code);

  List<Object[]> findCategoriesAndSize();
}
