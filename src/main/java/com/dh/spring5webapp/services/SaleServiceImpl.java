package com.dh.spring5webapp.services;


import com.dh.spring5webapp.model.Sale;
import com.dh.spring5webapp.repositories.SaleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SaleServiceImpl extends GenericServiceImpl<Sale> implements SaleService {
    private static Logger logger = LoggerFactory.getLogger(SaleServiceImpl.class);

    private SaleRepository repository;

    public SaleServiceImpl(SaleRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Sale, Long> getRepository() {
        return repository;
    }


    public List<Object[]> findBestSeller(int id){
        return repository.findBestSeller(Long.valueOf(id));
    }
}