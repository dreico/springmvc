<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="include/header.jsp"/>
<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <hr class="my-4">
                <form:form id="formLogin" method="post" modelAttribute="command" role="form" action="${pageContext.request.contextPath}/login">
                    <fieldset>

                        <legend><strong>Login</strong></legend>
                        <hr class="my-4">

                        <c:if test = "${err!=null}">
                            <div class="alert alert-dismissible alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Error! </strong> ${err}
                            </div>
                        </c:if>

                        <div class="form-group">
                            <label for="loginName">Username</label>
                            <form:input path="loginName" class="form-control" id="loginName" aria-describedby="fnHelp"  placeholder="Username or Email" />
                            <div class="loginNameM" style="color:red" ></div>
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <form:password path="password" class="form-control" id="password" aria-describedby="lnHelp" placeholder="Password"/>
                            <div class="passwordM" style="color:red"  ></div>

                        </div>


                        <button type="submit" class="btn btn-primary">Login</button>
                        <a href="${pageContext.request.contextPath}/index" class="btn btn-secondary">Cancel</a>
                            <%--</form>--%>

                    </fieldset>
                    </form:form>

            </div>  <%--class="col-sm-4"--%>
            <div class="col-sm-4"></div>
            <br>
        </div> <!-- Fin del Row -->
    </div> <!-- Fin Container -->
</div> <!-- End Container -->

<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/js/jquery.easing-1.3.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/js/plugins.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>

<script type="text/javascript">



    $(document).ready(function() {


        var loginValidated = false;
        var passValidated = false;


        $('#loginName').on('input', function (evt) {

            var value = evt.target.value;
            console.log(value);

           validateName(value);

            return

        });//fin del email

        function validateName(value){
            if (value.length === 0 ){
                $('#loginName').parent(".form-group").removeClass("has-success");
                $('#loginName').parent(".form-group").addClass("has-danger");
                console.log("error");
                $('.loginNameM').html("Put your email or username please");
                loginValidated = false;

            }else{

                $('#loginName').parent(".form-group").removeClass("has-success");
                $('#loginName').parent(".form-group").addClass("has-danger");
                $('.loginNameM').html("");
                loginValidated=true;

            }
        }

        $('#password').on('input', function (evt) {
            var value = evt.target.value;
            validatePassword(value);
            return
        });

        function validatePassword(value){
            if (value.length === 0) {
                $('#password').parent(".form-group").removeClass("has-success");
                $('#password').parent(".form-group").addClass("has-danger");
                //console.log("vass vacio");
                $('.passwordM').html("Put your password");
                passValidated = false;
            }else{
                console.log($('#password').val());
                $('#password').parent(".form-group").removeClass("has-danger");
                $('#password').parent(".form-group").addClass("has-success");
                $('.passwordM').html("");
                passValidated = true;
            }
        }

        $('#formLogin').submit(function(event) {


            validateName($('#loginName').val());
            validatePassword($('#password').val());

            if(!loginValidated || !passValidated){
                event.preventDefault(); //prevent the default action
            }

        });


    });

</script>

</body>
</html>