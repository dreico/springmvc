<jsp:include page="include/header.jsp"/>
<jsp:include page="include/menu.jsp"/>
<body>
<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-6">
                <h3 class="widget-title">Contact Us</h3>
                <div class="contact-form">
                    <form name="contactform" id="contactform" action="#" method="post">
                        <p>
                            <input name="name" type="text" id="name" placeholder="Your Name">
                        </p>
                        <p>
                            <input name="email" type="text" id="email" placeholder="Your Email">
                        </p>
                        <p>
                            <input name="subject" type="text" id="subject" placeholder="Subject">
                        </p>
                        <p>
                            <textarea name="message" id="message" placeholder="Message"></textarea>
                        </p>
                        <input type="submit" class="mainBtn" id="submit" value="Send Message">
                    </form>
                </div> <!-- /.contact-form -->
            </div>
            <div class="col-md-7 col-sm-6 map-wrapper">
                <h3 class="widget-title">Our Location</h3>
                <div class="map-holder" style="height: 360px"></div>
            </div>
        </div>
    </div>
</div> <!-- /.content-section -->

<jsp:include page="include/footer.jsp"/>

<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/js/jquery.easing-1.3.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/js/plugins.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>



<!-- Google Map -->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="${pageContext.request.contextPath}/js/vendor/jquery.gmap3.min.js"></script>

<!-- Google Map Init-->
<script type="text/javascript">
    jQuery(function($){
        $('.first-map, .map-holder').gmap3({
            marker:{
                address: '40.7828839,-73.9652425'
            },
            map:{
                options:{
                    zoom: 15,
                    scrollwheel: false,
                    streetViewControl : true
                }
            }
        });
    });
</script>







