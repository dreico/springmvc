<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/menu.jsp"/>



<script>

   // $('#carouselFront').css('cursor', 'pointer');
    function getItem(id){
        //$('#divid').css('cursor','pointer');
        var source = $("#dropdown").text();
        window.location.href='/details?id='+id;

    }
</script>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<s:if test="${errLogIsNeeded != null}">
    <div class="alert alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Oh snap!</strong> <a href="#" class="alert-link">Error!</a> ${errLogIsNeeded}
        (<a href="<c:url value='/login' />" >click to login</a>)
    </div>
</s:if>




<%--<table class="table table-light table-hover">--%>
    <%--<thead>--%>
    <%--<tr>--%>
        <%--<th scope="col">Id</th>--%>
        <%--<th scope="col">Code</th>--%>
        <%--<th scope="col">Name</th>--%>
        <%--<th scope="col">Image</th>--%>
        <%--<th scope="col">Operations</th>--%>
    <%--</tr>--%>
    <%--</thead>--%>
    <%--<tbody>--%>


    <%--<c:forEach var="item" items="${itemsNew}">--%>
        <%--<tr class="table-active">--%>
            <%--<th scope="row">${item.id}</th>--%>
            <%--<td>${item.code}</td>--%>
            <%--<td>${item.name}</td>--%>
            <%--<td>--%>

                <%--<c:choose>--%>
                    <%--<c:when test="${item.image == null}">--%>
                        <%--<img src="<c:url value='/images/avatar.jpeg' />" width="80" height="80"/>--%>
                    <%--</c:when>--%>
                    <%--<c:otherwise>--%>
                        <%--<img src="<c:url value='/items/${item.id}/readimage' />" width="80" height="80"/>--%>
                    <%--</c:otherwise>--%>
                <%--</c:choose>--%>



                <%--<a href="<c:url value='/items/${item.id}/image' />" class="btn btn-secondary">Change Image</a>--%>
            <%--</td>--%>
            <%--<td>--%>

                <%--<a href="<c:url value='/items/update/${item.id}' />" class="btn btn-warning">Edit</a>--%>
                <%--<br>--%>
                <%--<a href="<c:url value='/items/delete/${item.id}' />" class="btn btn-danger">Delete</a>--%>
            <%--</td>--%>
        <%--</tr>--%>
    <%--</c:forEach>--%>

    <%--</tbody>--%>
<%--</table>--%>


<div class="container">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" style=" width:100%; height: 300px !important;">
        <c:set var = "active"  value = "true"/>



        <c:forEach var="item" items="${itemsDiscount}">

            <c:choose>
                <c:when test="${active==true}">
                    <div class="item active" style="height: 100%;">
                    <c:set var="active" value = "false"/>
                </c:when>
                <c:otherwise>
                        <%--NOT ACTIVE ITEM--%>
                    <div class="item" style="height: 100%;" >
                </c:otherwise>
            </c:choose>

                    <c:choose>
                        <c:when test="${item.image == null}">

                            <img src="${pageContext.request.contextPath}/images/item-default.png" alt="Chania" class="img-responsive center-block" />
                        </c:when>
                        <c:otherwise>
                            <img src="<c:url value='/items/${item.id}/readimage' />" class="img-responsive center-block" style="height: 100%;"/>
                        </c:otherwise>
                    </c:choose>


                        <div class="carousel-caption" id="carouselFront"  onclick="getItem(${item.id})" style="cursor: pointer" title="More Information">
                            <p class="lead"><strong>${item.name}</strong></p>
                            <div style="height: 75px;"></div>
                            <%--<p class="text-muted"><h5><em>${item.code}</em></h5></p>--%>
                            <%--<p><h5><em>${item.detail}</em></h5></p>--%>
                            <%--<p><h3>${item.discount}% Discount</h3></p>--%>
                            <div style="width: 100%;font-size: 24px"><strong>-${item.discount}% Desc</strong><img src="${pageContext.request.contextPath}/images/offer.png" width="80px" height="80px" alt="${item.name}" /></div>

                            <%--<p><a href="#" class="main-btn">Buy Now</a></p>--%>
                        </div>
                    </div><!--FIN DEL DIV ACTIVE-->

        </c:forEach>



    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
</div>
</div><%--FIN DEL CONTAINER--%>



<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title">
                <h2>New Products</h2>
            </div> <!-- /.section -->
        </div> <!-- /.row -->
        <div class="row">

            <c:forEach var="item" items="${itemsNew}">
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="product-thumb">

                            <c:choose>
                                <c:when test="${item.image == null}">
                                    <img src="${pageContext.request.contextPath}/images/item-default.png" alt="Chania"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="<c:url value='/items/${item.id}/readimage' />"/>
                                </c:otherwise>
                            </c:choose>

                        </div> <!-- /.product-thum -->
                        <div class="product-content">
                            <h5><a href="<c:url value='/details?id=${item.id}' />">${item.name}</a></h5>
                            <p>${item.detail} </p>
                            <span class="price">$${item.price}</span>
                        </div> <!-- /.product-content -->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
            </c:forEach>

        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.content-section -->



<jsp:include page="include/footer.jsp"/>


<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/js/jquery.easing-1.3.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/js/plugins.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>