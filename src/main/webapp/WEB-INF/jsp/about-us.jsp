<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/menu.jsp"/>
<body>

<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8">


                <div class="product-information">
                    <h2>About us</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, esse, excepturi, sint ut et numquam reiciendis nulla in deserunt quaerat provident obcaecati reprehenderit iusto ab neque corporis id temporibus architecto quia adipisci? Officia, aliquam, eveniet, molestias, voluptate porro assumenda error soluta ab blanditiis voluptatum nisi voluptates debitis doloribus. Asperiores, provident fuga quibusdam id tenetur!<br><br> <img src="${pageContext.request.contextPath}/images/shopping.jpg" class="img-rounded" alt="Cinque Terre" style="height: 50%;width: 50%;"> <br><br>Nostrum quis quo earum enim suscipit molestiae cupiditate reprehenderit? Atque, numquam nostrum adipisci suscipit exercitationem sed ullam. Odio, laborum, obcaecati harum nostrum pariatur ipsam itaque minima atque expedita at amet dignissimos odit quisquam laboriosam eius officiis ratione alias sint rerum distinctio. Quidem, veritatis consequuntur voluptas sunt quo deleniti reprehenderit deserunt atque minus non.</p>
                    <p class="product-infos">

                    </p>
                    <ul class="product-buttons">

                        <blockquote class="blockquote text-center">
                            <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                            <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                        </blockquote>

                    </ul>
                </div> <!-- /.product-information -->
            </div> <!-- /.col-md-8 -->
            <div class="col-md-4 col-sm-8">
                <h2>Blog</h2>
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h3 class="mb-1">Look for the Lock</h3>
                            <small>3 days ago</small>
                        </div>
                        <p class="mb-1">Never ever, ever buy anything online using your credit card from a site that doesn't have SSL (secure sockets layer) encryption installed—at the very least. </p>
                        <small>Donec id elit non mi porta.</small>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h3 class="mb-1"> Don't Tell All</h3>
                            <small class="text-muted">1 week ago</small>
                        </div>
                        <p class="mb-1">No online shopping store needs your social security number or your birthday to do business. </p>
                        <small class="text-muted">Donec id elit non mi porta.</small>
                    </a>

                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h3 class="mb-1">Check Statements</h3>
                            <small>3 days ago</small>
                        </div>
                        <p class="mb-1">Don't wait for your bill to come at the end of the month. Go online regularly during the holiday season and look at electronic statements for your credit card,</p>
                        <small>Donec id elit non mi porta.</small>
                    </a>

                </div>
                <div>
                    <h2>Popular Searchs</h2>

                    <span class="badge badge-pill badge-primary">Tv</span>
                    <span class="badge badge-pill badge-secondary">radio</span>
                    <span class="badge badge-pill badge-success">samsung</span>
                    <span class="badge badge-pill badge-danger">aiwa</span>
                    <span class="badge badge-pill badge-warning">stereo</span>
                    <span class="badge badge-pill badge-info">plasma</span>
                    <span class="badge badge-pill badge-light">sony</span>
                    <span class="badge badge-pill badge-dark">Dark</span>
                </div>

            </div> <!-- /.col-md-4 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.content-section -->

<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title">
                <h2>Vote For Future Products</h2>
            </div> <!-- /.section -->
        </div> <!-- /.row -->
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="product-item-vote">
                    <div class="product-thumb">
                        <img src="images/products/1.jpg" alt="">
                    </div> <!-- /.product-thum -->
                    <div class="product-content">
                        <h5><a href="#">Name of Shirt</a></h5>
                        <span class="tagline">By: Catherine</span>
                        <ul class="progess-bars">
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;"></div>
                                    <span>4<i class="fa fa-heart"></i></span>
                                </div>
                            </li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar comments" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                                    <span class="comments">6<i class="fa fa-heart"></i></span>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- /.product-content -->
                </div> <!-- /.product-item-vote -->
            </div> <!-- /.col-md-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="product-item-vote">
                    <div class="product-thumb">
                        <img src="images/products/2.jpg" alt="">
                    </div> <!-- /.product-thum -->
                    <div class="product-content">
                        <h5><a href="#">Name of Shirt</a></h5>
                        <span class="tagline">By: Rebecca</span>
                        <ul class="progess-bars">
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;"></div>
                                    <span>4<i class="fa fa-heart"></i></span>
                                </div>
                            </li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar comments" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                                    <span class="comments">6<i class="fa fa-heart"></i></span>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- /.product-content -->
                </div> <!-- /.product-item-vote -->
            </div> <!-- /.col-md-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="product-item-vote">
                    <div class="product-thumb">
                        <img src="images/products/3.jpg" alt="">
                    </div> <!-- /.product-thum -->
                    <div class="product-content">
                        <h5><a href="#">Name of Shirt</a></h5>
                        <span class="tagline">By: Catherine</span>
                        <ul class="progess-bars">
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;"></div>
                                    <span>4<i class="fa fa-heart"></i></span>
                                </div>
                            </li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar comments" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                                    <span class="comments">6<i class="fa fa-heart"></i></span>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- /.product-content -->
                </div> <!-- /.product-item-vote -->
            </div> <!-- /.col-md-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="product-item-vote">
                    <div class="product-thumb">
                        <img src="images/products/4.jpg" alt="">
                    </div> <!-- /.product-thum -->
                    <div class="product-content">
                        <h5><a href="#">Name of Shirt</a></h5>
                        <span class="tagline">By: Rebecca</span>
                        <ul class="progess-bars">
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;"></div>
                                    <span>4<i class="fa fa-heart"></i></span>
                                </div>
                            </li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar comments" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                                    <span class="comments">6<i class="fa fa-heart"></i></span>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- /.product-content -->
                </div> <!-- /.product-item-vote -->
            </div> <!-- /.col-md-3 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.content-section -->

<jsp:include page="include/footer.jsp"/>



<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/js/jquery.easing-1.3.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/js/plugins.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>


