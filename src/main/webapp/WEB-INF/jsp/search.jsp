<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/menu.jsp"/>

<style type="text/css">
    a:hover {
        color: #d6d8db;
    }
</style>

<body>



<%--##################### Categories ################################--%>
<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel-group">
                    <h3>Categories</h3>

                    <c:forEach var="category" items="${categories}">
                        <c:set var = "id"  value = "${category[0]}"/>
                        <c:set var = "name"  value = "${category[1]}"/>
                        <c:set var = "numberSubItems"  value = "${category[2]}"/>

                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #2a80b9;color:white;">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" class="category" href="#collapse${id}" onclick="getSubcategories(${id});">${name} </a>
                                    <span class="badge badge-primary badge-pill">+${numberSubItems}</span>
                                </h4>
                            </div>
                            <div id="collapse${id}" class="panel-collapse collapse">
                                    <%--<div class="panel-body"><a href="#"><strong>sub </strong>(20)</a></div>--%>
                                <a class="urlSearch" href="<c:url value='/search' />"></a>
                            </div>
                        </div>
                    </c:forEach>
                    <img id="loading-image" src="${pageContext.request.contextPath}/images/ajax-loader.gif" style="display:none;width:70px;height: 70px;"/>


                </div>
            </div> <!-- /.col-md-3 -->

<%--####################################### Item Random #####################################--%>

            <div class="col-md-9">
                <c:forEach var="item" items="${itemPaging.content}">
                    <c:set var = "id"  value = "${item[0]}"/>
                    <c:set var = "code"  value = "${item[4]}"/>
                    <c:set var = "description"  value = "${item[5]}"/>
                    <c:set var = "detail"  value = "${item[6]}"/>
                    <c:set var = "discount"  value = "${item[7]}"/>
                    <c:set var = "image"  value = "${item[8]}"/>
                    <c:set var = "name"  value = "${item[9]}"/>
                    <c:set var = "price"  value = "${item[10]}"/>



                        <div class="col-md-4" style="height: 250px">
                            <div class="product-item-2" style="height: 250px;">
                                <div class="product-thumb">

                                    <c:choose>
                                        <c:when test="${image == null}">
                                            <img src="${pageContext.request.contextPath}/images/item-default.png" alt="${name}" class="img-responsive center-block" />
                                        </c:when>
                                        <c:otherwise>
                                            <img src="<c:url value='/items/${id}/readimage' />" class="img-responsive center-block" style="height: 100%;" alt="${name}"/>
                                        </c:otherwise>
                                    </c:choose>


                                </div> <!-- /.product-thumb -->
                                <div class="product-content overlay">
                                    <h5><a href="<c:url value='/details?id=${id}' />">${name}</a></h5>
                                    <span class="tagline">${code}</span>
                                    <span class="price">$${price}</span>
                                    <p>${description}</p>
                                </div> <!-- /.product-content -->
                            </div> <!-- /.product-item-2 -->

                            <div class="clearfix"></div>
                        </div> <!-- /.product-holder -->
                </c:forEach>

            </div> <!--colum about 9-->


        </div> <!-- /.row -->


<%--####################### Pagination BAR ########################--%>
        <%--Verify if subcategory id exist--%>

        <div class="container">
            <div class="row">
                <div align="center">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="<c:url value='/search?page=0&sub=${idSubCategory}' />">&laquo;</a>
                        </li>




                        <%--buiding link bars--%>
                        <c:forEach var="i" begin="1" end="${itemPaging.totalPages}" step="1">


                                <c:choose>
                                    <c:when test="${active==true}">
                                        <li class="page-item active">
                                        <c:set var="active" value = "false"/>
                                    </c:when>
                                    <c:otherwise>
                                            <%--NOT ACTIVE ITEM--%>
                                        <li class="page-item">
                                    </c:otherwise>
                                </c:choose>
                                            <a class="page-link" href="<c:url value='/search?page=${i-1}&sub=${idSubCategory}' />">${i}</a>
                                        </li>



                        </c:forEach>




                        <li class="page-item">
                            <a class="page-link" href="<c:url value='/search?page=${itemPaging.totalPages-1}&sub=${idSubCategory}' />">&raquo;</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


        <%--################ item random pluss #########################--%>
        <div class="row">

            <c:forEach var="item" items="${itemsRandomPlus}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="product-item-4" style="height: 360px;">
                        <div class="product-thumb">

                                <c:choose>
                                    <c:when test="${item.image == null}">
                                        <img src="${pageContext.request.contextPath}/images/item-default.png" alt="${item.name}" class="img-responsive center-block" style="height: 100%;" />
                                    </c:when>
                                    <c:otherwise>
                                        <img src="<c:url value='/items/${item.id}/readimage' />" class="img-responsive center-block" style="height: 100%;" alt="${item.name}"/>
                                    </c:otherwise>
                                </c:choose>
                        </div> <!-- /.product-thumb -->
                        <div class="product-content overlay">
                            <h5><a href="<c:url value='/details?id=${item.id}' />">${item.name}</a></h5>
                            <span class="tagline">${item.detail}</span>
                            <span class="price">$${item.price}</span>
                            <p>${item.description}</p>
                        </div> <!-- /.product-content -->
                    </div> <!-- /.product-item-4 -->
                </div> <!-- /.col-md-4 -->
            </c:forEach>



        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.content-section -->



<!-- ###############  Container  New Products #############-->
<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title">
                <h2>Most Popular Products</h2>
            </div> <!-- /.section -->
        </div> <!-- /.row -->
        <div class="row">

            <c:forEach var="item" items="${itemsBestSeller}">
            <%--<p>${item[0]}</p>--%>
            <%--<p>    ${item[1]}</p>--%>
            <%--<p> ${item[2]}</p>--%>
            <%--<p> ${item[3]}</p>--%>
            <%--<p> ${item[4]}</p>--%>

                    <div class="col-md-3 col-sm-6">
                        <div class="product-item" >
                            <div style=" height:250;width:100%;text-align: center; line-height: 250px;">
                                    <c:choose>
                                        <c:when test="${item[4] == null}">
                                            <img src="${pageContext.request.contextPath}/images/item-default.png" alt="${item[1]}"  style="width: 100%;    vertical-align: middle;"  title="${item[1]}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img src="<c:url value='/items/${item[0]}/readimage' />"  style="width: 100%; vertical-align: middle;" alt="${item[1]}" title="${item[1]}"/>
                                        </c:otherwise>
                                    </c:choose>
                            </div> <!-- /.product-thum -->
                            <div class="product-content">
                                <h5><a href="<c:url value='/details?id=${item[0]}' />">${item[1]}</a></h5>
                                <span class="price">$${item[3]}</span>
                            </div> <!-- /.product-content -->
                        </div> <!-- /.product-item -->
                    </div> <!-- /.col-md-3 -->

            </c:forEach>

        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.content-section -->


<jsp:include page="include/footer.jsp"/>



<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/js/jquery.easing-1.3.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/js/plugins.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>

<script type="text/javascript">
    // function crunchifyAjax() {
    //     //alert("click !!!");
    //     var obj = "id=1";
    //     $.ajax({
    //         url : 'ajaxtest',
    //         type:"GET",
    //         data:obj,
    //         success : function(data) {
    //             $('#result').html(data);
    //             console.log(data);
    //             for (var i=0;i<data.length;i++){
    //                 console.log(data[i].id);
    //                 var c = "<p>"+data[i].id+"</p>"
    //                 $('#result').append(c);
    //             }
    //         }//fin success
    //     });
    // }

    function getSubcategories(id){

        var obj = "id="+id;
        $.ajax({
            url : 'getSubcategories',
            type:"GET",
            data:obj,
            beforeSend: function() {
                // $("div-loading-"+id).append($("#loading-image"));
                // $("div-loading-"+id).append("hello !!!");
                $("#loading-image").clone().appendTo("#collapse"+id).show();

                //$("#loading-image").show();
            },
            success : function(data) {
                $('#collapse'+id).html("");

                console.log(data);

                for (var i=0;i<data.length;i++){
                    var idSubCategory = data[i][0];
                    var name = data[i][1];
                    var items = data[i][2];
                    // console.log(data[i][0]);
                    // console.log(data[i][1]);
                    // var link = jQuery('<a>').attr('href', direction).text('<strong>'+name+'</strong>('+items+')');
                    // var div = $('<div class="panel-body"></div>').append(link);

                    var link = '<a href="'+$('.urlSearch').attr('href')+'?sub='+idSubCategory+'"><strong>'+name+' </strong>('+items+')</a>';
                    var c = '<div class="panel-body">'+link+'</div>';
                    $('#collapse'+id).append(c);

                }
            }//fin success
        });
    }
</script>

