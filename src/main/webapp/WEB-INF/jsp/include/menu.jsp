<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${loggedEmployee == null}">


<header class="site-header">
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="top-header-left">
                        <a href="#">Blog</a>
                        <a href="#">Partners</a>
                    </div> <!-- /.top-header-left -->
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6">
                    <div class="social-icons">
                        <ul>
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-dribbble"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-linkedin"></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div> <!-- /.social-icons -->
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.top-header -->
    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-8">
                    <div class="logo">
                        <!-- <h1><a href="#">Kool Store</a></h1> -->
                        <image src="${pageContext.request.contextPath}/images/indice.png" style="max-width: 100%; height: auto;"/>
                    </div> <!-- /.logo -->
                </div> <!-- /.col-md-4 -->
                <div class="col-md-9 col-sm-6 col-xs-4">

                </div> <!-- /.col-md-8 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.main-header -->
    <div class="main-nav">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-7">
                    <div class="list-menu">
                        <ul>
                            <li><a href="<c:url value='/' />">Home</a></li>
                            <li><a href="<c:url value='/catalog' />">Catalog</a></li>
                            <li><a href="<c:url value='/about-us' />">About us</a></li>
                            <li><a href="<c:url value='/contact' />">Contact</a></li>
                        </ul>
                    </div> <!-- /.list-menu -->
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-5">
                    <div class="notification">
                        <span>Free Shipping on any order above $50</span>
                    </div>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.main-nav -->


</header> <!-- /.site-header -->

    </c:when>
    <c:otherwise>

<header class="site-header">
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="top-header-left">
                        <a href="#">Blog</a>
                        <a href="#">Partners</a>
                    </div> <!-- /.top-header-left -->
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6">
                    <div class="social-icons">
                        <ul>
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-dribbble"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-linkedin"></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div> <!-- /.social-icons -->
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.top-header -->
    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-8">
                    <div class="logo">
                        <!-- <h1><a href="#">Kool Store</a></h1> -->
                        <image src="${pageContext.request.contextPath}/images/indice.png" style="max-width: 100%; height: auto;"/>
                    </div> <!-- /.logo -->
                </div> <!-- /.col-md-4 -->
                <div class="col-md-9 col-sm-6 col-xs-4">

                </div> <!-- /.col-md-8 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.main-header -->
    <div class="main-nav">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-7">
                    <div class="list-menu">
                        <ul>
                            <li><a href="<c:url value='/' />">Home</a></li>
                            <li><a href="<c:url value='/catalog' />">Catalog</a></li>
                            <li><a href="<c:url value='/about-us' />">About us</a></li>
                            <li><a href="<c:url value='/contact' />">Contact</a></li>
                            <li><a href="#">
                                <img src="${pageContext.request.contextPath}/images/admin.png" width="30px" height="30px" />
                            </a></li>
                            <li><a href="<c:url value='/items' />">
                                <img src="${pageContext.request.contextPath}/images/item.ico" width="30px" height="30px" /> Items
                            </a></li>
                            <li><a href="<c:url value='/employees' />">
                                <img src="${pageContext.request.contextPath}/images/users.png" width="30px" height="30px" />Users
                            </a></li>
                        </ul>
                    </div> <!-- /.list-menu -->
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-5">
                    <div class="notification">
                        <span>Welcome! Your are logged us
                            <h5 style="text-transform: uppercase;">${loggedEmployee.firstName} ${loggedEmployee.lastName}</h5>
                        </span>
                    </div>
                    <div class="list-menu" align="right">
                        <ul>
                            <li><a href="<c:url value='/logout' />">
                                <img src="${pageContext.request.contextPath}/images/logout.ico" width="30px" height="30px" />Logout
                            </a></li>

                        </ul>
                    </div> <!-- /.list-menu -->
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.main-nav -->





</header> <!-- /.site-header -->


    </c:otherwise>
</c:choose>
