<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include/header.jsp"/>
<jsp:include page="include/menu.jsp"/>
<body>

<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <div class="product-image" style="width:100%; height: 300px ;  text-align: center; line-height: 300px;">


                    <c:choose>
                        <c:when test="${item.image == null}">
                            <img src="${pageContext.request.contextPath}/images/item-default.png" alt="${item.name}" class="img-responsive center-block" style="height: 100%;width: auto;"/>
                        </c:when>
                        <c:otherwise>
                            <img src="<c:url value='/items/${item.id}/readimage' />" class="img-responsive center-block" alt="${item.name}" style="height: 100%;width: auto;"/>
                        </c:otherwise>
                    </c:choose>


                </div> <!-- /.product-thum -->

                <div class="product-information">
                    <h2>${item.name}</h2>
                    <button type="button" class="btn btn-warning">${item.code}</button>
                    <p>${item.detail}</p>
                    <p>${item.description}</p>
                    <p class="product-infos">
                        <span>Bundle Price: $${item.price}</span>
                        <span>Discount: ${item.discount}%</span>
                    </p>
                    <ul class="product-buttons">
                        <li>
                            <a href="#" class="main-btn">Buy Now</a>
                        </li>
                        <li>
                            <a href="#" class="main-btn">Add to Cart</a>
                        </li>
                    </ul>
                </div> <!-- /.product-information -->
            </div> <!-- /.col-md-8 -->
            <div class="col-md-4 col-sm-8">


                <c:forEach var="item" items="${itemsRandomPlus}">
                        <div class="product-item-2" >
                            <div class="product-thumb" style="height: 250px;">
                                    <c:choose>
                                        <c:when test="${item.image == null}">
                                            <img src="${pageContext.request.contextPath}/images/item-default.png" alt="${item.name}" class="img-responsive center-block" />
                                        </c:when>
                                        <c:otherwise>
                                            <img src="<c:url value='/items/${item.id}/readimage' />" class="img-responsive center-block" style="height: 100%;width:auto;" alt="${item.name}"/>
                                        </c:otherwise>
                                    </c:choose>
                            </div> <!-- /.product-thumb -->
                            <div class="product-content overlay">
                                <h5><a href="<c:url value='/details?id=${item.id}' />">${item.name}</a></h5>
                                <span class="tagline">${item.code}</span>
                                <span class="price">$${item.price}</span>
                                <p>${item.detail}</p>
                            </div> <!-- /.product-content -->
                        </div> <!-- /.product-item-2 -->
                </c:forEach>





            </div> <!-- /.col-md-4 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.content-section -->


<jsp:include page="include/footer.jsp"/>



<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/js/jquery.easing-1.3.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/js/plugins.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>


