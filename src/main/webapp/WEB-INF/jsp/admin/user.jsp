<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="../include/header.jsp"/>
<jsp:include page="../include/menu.jsp"/>

<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <hr class="my-4">
                <%--<form:form id="form" method="post" modelAttribute="employee" role="form" action="${pageContext.request.contextPath}/employee">--%>
                <form action="${pageContext.request.contextPath}/employee" method="post">
                    <fieldset>

                        <c:choose>
                            <c:when test="${newUser}">
                                <legend><strong>Create User</strong></legend>
                            </c:when>
                            <c:otherwise>
                                <legend><strong>Edit User</strong></legend>
                            </c:otherwise>
                        </c:choose>

                        <hr class="my-4">

                        <c:if test = "${err!=null}">
                            <div class="alert alert-dismissible alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Warning!</strong> ${err}
                            </div>
                        </c:if>

                        <div class="form-group">

                            <label>First Name</label>

                            <%--<form:input type="hidden" path="id"/>--%>
                            <%--<form:input type="hidden" path="version"/>--%>

                            <input type="hidden" id="formImage" name="id" value="${employee.id}">
                            <input type="hidden" id="formImage" name="version" value="${employee.version}">


                            <input class="form-control" id="firstName" name="firstName" aria-describedby="fnHelp" placeholder="Enter first name" type="text" value="${employee.firstName}"/>
                            <div id="firstNameM" class="invalid-feedback" style="color:red"></div>
                            <small id="fnHelp" class="form-text text-muted">Advice: Insert at least 3 characters.</small>

                        </div>
                        <div class="form-group">
                            <label >Last Name</label>

                            <input class="form-control" id="lastName" name="lastName" aria-describedby="lnHelp" placeholder="Enter last name" type="text" value="${employee.lastName}">
                            <div id="lastNameM" class="invalid-feedback" style="color:red"></div>
                            <small id="lnHelp" class="form-text text-muted">Advice: Insert at least 3 characters.</small>
                        </div>

                        <%--<div class="form-group">--%>
                            <%--<label >Change Avatar</label>--%>
                            <%--<a href="<c:url value='/employees/${id}/image' />" class="btn btn-secondary">Change Image</a>--%>
                            <%--<small class="form-text text-muted">We'll never share your last name with anyone else.</small>--%>
                            <%--<div>--%>



                            <%--</div>--%>
                        <%--</div>--%>

                        <button type="submit" id="submitForm" class="btn btn-primary">Save</button>
                        <a href="<c:url value='/employees' />" class="btn btn-secondary">Cancel</a>
                    </fieldset>
                </form>
                <%--</form:form>--%>

            </div>  <%--class="col-sm-4"--%>
            <div class="col-sm-4"></div>
            <br>
        </div> <!-- Fin del Row -->
    </div> <!-- Fin Container -->
</div> <!-- End Container -->
<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/js/jquery.easing-1.3.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/js/plugins.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>

<script>
    $(document).ready(function() {



        var firstNameValidated = false;
        var lastNameValidated = false;



        $('#firstName').on('input', function (evt) {
            var value = evt.target.value;
            console.log(value);
            validateFirstName(value);

            return
        });//fin

        function validateFirstName(value){
            if(value == null){
                console.log("null name");
                return;
            }
            if ( value.length === 0 ) {
                $('#firstNameM').parent(".form-group").removeClass("has-success");
                $('#firstNameM').parent(".form-group").addClass("has-danger");
                $('#firstNameM').html("Insert user's name");
                firstNameValidated = false;
            }else{

                $('#firstNameM').parent(".form-group").removeClass("has-success");
                $('#firstNameM').parent(".form-group").addClass("has-danger");
                $('#firstNameM').html("");
                firstNameValidated = true;
            }
        }

        $('#lastName').on('input', function (evt) {

            var value = evt.target.value;
            console.log(value);
            validateLastName(value);

            return
        });//end

        function validateLastName(value){
            if(value == null){
                console.log("null");
                return;
            }

            if ( value.length === 0) {
                $('#lastNameM').parent(".form-group").removeClass("has-success");
                $('#lastNameM').parent(".form-group").addClass("has-danger");
                $('#lastNameM').html("Insert user's lastname");
                lastNameValidated = false;
            }else{

                $('#lastNameM').parent(".form-group").removeClass("has-success");
                $('#lastNameM').parent(".form-group").addClass("has-danger");
                $('#lastNameM').html("");
                lastNameValidated = true;
            }
        }


        $("#submitForm").on('click',  function(e) {
            validateFirstName($('#firstName').val());
            validateLastName($('#lastName').val());

            console.log(firstNameValidated);
            console.log(lastNameValidated);
            if(firstNameValidated && lastNameValidated){

            }else{
                e.preventDefault(); //prevent the default action
            }

        });


    });


</script>

<jsp:include page="../include/footer.jsp"/>



