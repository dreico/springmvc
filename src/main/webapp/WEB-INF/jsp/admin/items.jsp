<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../include/header.jsp"/>
<jsp:include page="../include/menu.jsp"/>

<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">

                <legend><strong>Item List</strong></legend>
                <hr class="my-4">
                <p>${formImg}</p>
                <a href="${pageContext.request.contextPath}/items/new" class="btn btn-primary">New Item</a>
                <br>
                <table class="table table-light table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Code</th>
                        <th scope="col">Name</th>
                        <th scope="col">Image</th>
                        <th scope="col">Operations</th>
                    </tr>
                    </thead>
                    <tbody>
                    <p>The value of the var is the next : ${loggedEmployee.lastName}</p>

                    <c:forEach var="item" items="${items}">
                        <tr class="table-active">
                            <th scope="row">${item.id}</th>
                            <td>${item.code}</td>
                            <td>${item.name}</td>
                            <td>

                                <c:choose>
                                    <c:when test="${item.image == null}">
                                        <img src="<c:url value='/images/avatar.jpeg' />" width="80" height="80"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img src="<c:url value='/items/${item.id}/readimage' />" width="80" height="80"/>
                                    </c:otherwise>
                                </c:choose>



                                <a href="<c:url value='/items/${item.id}/image' />" class="btn btn-secondary">Change Image</a>
                            </td>
                            <td>

                                <a href="<c:url value='/items/update/${item.id}' />" class="btn btn-warning">Edit</a>
                                <br>
                                <a href="<c:url value='/items/delete/${item.id}' />" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>

                    </tbody>
                </table>


            </div>  <%--class="col-sm-4"--%>
            <div class="col-sm-3"></div>
            <br>
        </div> <!-- Fin del Row -->
    </div> <!-- Fin Container -->
</div> <!-- End Container -->



<jsp:include page="../include/footer.jsp"/>
