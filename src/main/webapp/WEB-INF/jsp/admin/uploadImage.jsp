<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="../include/header.jsp"/>
<jsp:include page="../include/menu.jsp"/>


<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <hr class="my-4">
                <form method="post" enctype="multipart/form-data" action="<c:url value='/employees/${employee.id}/image' />">
                    <label class="control-label">Select File</label>
                    <input id="imagefile" name="imagefile" type="file" class="file"/>
                    <button type="submit">Submit</button>
                </form>

            </div>  <%--class="col-sm-4"--%>
            <div class="col-sm-4"></div>
            <br>
        </div> <!-- Fin del Row -->
    </div> <!-- Fin Container -->
</div> <!-- End Container -->




<jsp:include page="../include/footer.jsp"/>
