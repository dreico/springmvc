<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="../include/header.jsp"/>
<jsp:include page="../include/menu.jsp"/>


<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <hr class="my-4">
                <%--<form:form id="form" method="post" modelAttribute="employee" role="form" action="${pageContext.request.contextPath}/employee">--%>
                <form action="${pageContext.request.contextPath}/items" method="post">
                    <fieldset>

                        <c:choose>
                            <c:when test="${newItem}">
                                <legend><strong>Create Item</strong></legend>

                            </c:when>
                            <c:otherwise>
                                <legend><strong>Edit Item</strong></legend>
                            </c:otherwise>
                        </c:choose>

                        <hr class="my-4">

                        <c:if test = "${err!=null}">
                            <div class="alert alert-dismissible alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Warning!</strong> ${err}
                            </div>
                        </c:if>

                        <div class="form-group">

                            <label >Item Name</label>

                            <input type="hidden"  name="id" value="${item.id}">
                            <input type="hidden"  name="version" value="${item.version}">


                            <input class="form-control" id="name" name="name" aria-describedby="fnHelp" placeholder="Enter Item name" type="text" value="${item.name}"/>
                            <div id="nameM" class="invalid-feedback" style="color:red"></div>
                            <small id="fnHelp" class="form-text text-muted">Advice:Insert a name with at less 5 characters.</small>

                        </div>
                        <div class="form-group">
                            <label >Item Code</label>
                            <%--<form:input path="lastName" class="form-control" id="lastName" aria-describedby="lnHelp" placeholder="Enter last name" type="text"/>--%>
                            <input class="form-control" id="code" name="code" aria-describedby="lnHelp" placeholder="Enter Item Code" type="text" value="${item.code}">
                            <div id="codeM" class="invalid-feedback" style="color:red"></div>
                            <small id="lnHelp" class="form-text text-muted">Advice: Inser a code with at less 3 characters.</small>
                        </div>
                        <div id="ptm"></div>
                        <div class="form-group">

                            <label >SubCategories</label>
                            <P>Select item's subcategory : ${subcategoryId}</P>
                            <select class="form-control" name="subcategory" id="subcategory" >
                                <c:forEach var="sub" items="${subcategories}">

                                    <option value="${sub.id}"
                                        <c:if test="${!newItem && subcategoryId == sub.id}">
                                            selected="selected"
                                        </c:if>
                                            >${sub.name}-(${sub.code})</option>


                                </c:forEach>
                            </select>

                        </div>

                        <div class="form-group">
                            <label >Select Image</label>
                            <%--<form:input path="lastName" class="form-control" id="lastName" aria-describedby="lnHelp" placeholder="Enter last name" type="text"/>--%><label >SubCategories</label>
                            <c:choose>
                                <c:when test="${item.image == null}">
                                    <img src="<c:url value='/images/avatar.jpeg' />" width="80" height="80"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="<c:url value='/items/${item.id}/readimage' />" width="80" height="80"/>
                                </c:otherwise>
                            </c:choose>
                            <a href="<c:url value='/items/${item.id}/image' />" class="btn btn-secondary">Change Image</a>
                            <small id="lnHelp" class="form-text text-muted">Advice: Max image's size <strong>2 MB.</strong>.</small>
                        </div>

                        <button type="submit" class="btn btn-primary" id="submitForm" >Save</button>
                        <a href="<c:url value='/items' />" class="btn btn-secondary">Cancel</a>
                    </fieldset>
                </form>
                <%--</form:form>--%>

            </div>  <%--class="col-sm-4"--%>
            <div class="col-sm-4"></div>
            <br>
        </div> <!-- Fin del Row -->
    </div> <!-- Fin Container -->
</div> <!-- End Container -->

<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/js/jquery.easing-1.3.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/js/plugins.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>

<script>
    $(document).ready(function() {



        var nameValidated = false;
        var codeValidated = false;



        $('#name').on('input', function (evt) {
            var value = evt.target.value;
            console.log(value);
            validateName(value);

            return
        });//fin

        function validateName(value){
            if(value == null){
                console.log("null name");
                return;
            }
            if ( value.length === 0) {
                $('#nameM').parent(".form-group").removeClass("has-success");
                $('#nameM').parent(".form-group").addClass("has-danger");
                $('#nameM').html("Insert item's name");
                nameValidated = false;
            }else{

                $('#nameM').parent(".form-group").removeClass("has-success");
                $('#nameM').parent(".form-group").addClass("has-danger");
                $('#nameM').html("");
                nameValidated=true;
            }
        }

        $('#code').on('input', function (evt) {

            var value = evt.target.value;
            console.log(value);
            validateCode(value);
            enableSubmit();
            return
        });//end

        function validateCode(value){
            if(value == null){
                console.log("null");
                return;
            }

            if ( value.length === 0) {
                $('#codeM').parent(".form-group").removeClass("has-success");
                $('#codeM').parent(".form-group").addClass("has-danger");
                $('#codeM').html("Insert code's name");
                codeValidated = false;
            }else{

                $('#codeM').parent(".form-group").removeClass("has-success");
                $('#codeM').parent(".form-group").addClass("has-danger");
                $('#codeM').html("");
                codeValidated=true;
            }
        }


        $("#submitForm").on('click',  function(e) {
            validateName($('#name').val());
            validateCode($('#code').val());
            console.log(nameValidated);
            console.log(codeValidated);
            if(nameValidated && codeValidated){

            }else{
                e.preventDefault(); //prevent the default action
            }

        });


    });


</script>
<jsp:include page="../include/footer.jsp"/>



