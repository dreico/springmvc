
<jsp:include page="include/header.jsp"/>
<body>
<h3>hello includings !!</h3>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">About</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" placeholder="Search" type="text">
            <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>

<div class="container">
    <table class="table table-striped">
        <caption>Your todos are</caption>
        <thead>
        <tr>
            <th>Description</th>
            <th>Target Date</th>
            <th>Is it Done?</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Todo 1</td>
            <td>10/12/2017</td>
            <td>No</td>
            <td><a class="btn btn-warning" href="/edit-todo">Edit Todo</a></td>
            <td><a class="btn btn-warning" href="/delete-todo">Delete Todo</a></td>
        </tr>
        </tbody>
    </table>
    <div>
        <a class="btn btn-default" href="/add-todo">Add a Todo</a>

    </div>
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/custom.js"></script>
</div>
</body>
</html>
